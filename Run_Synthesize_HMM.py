import glob
import os
import shutil
import subprocess
import sys
import logging
import datetime
import zipfile
from win_unc import (UncDirectory, UncCredentials, UncDirectoryMount)
from win_unc.query import get_connection_for_unc_directory
import ConfigParser

main_folder = os.getcwd()
run_time = {}  # to save run time process for all lab files
testfolder_path = main_folder + '\\testFolder\\'
synthesis_path = main_folder + '\\synthesis\\'
# Connect to share folder
share_folder = r'\\10.1.10.8\share'

# import info from conf.txt
configParser = ConfigParser.RawConfigParser()
configFilePath = r'conf.txt'
configParser.read(configFilePath)
training_version = configParser.get('Config', 'Training Version')
training_sentences = configParser.get('Config', 'Training Sentences')
synthesize_version = configParser.get('Config', 'Synthesize Version')
type_t = configParser.get('Config', 'Type')
file_suffix = '.' + filter(str.isdigit, synthesize_version) + 'S' + type_t + '.nvc_'\
              + filter(str.isdigit, training_version) + type_t + '_' + training_sentences
folder_name = 'V' + filter(str.isdigit, training_version) + '_' + type_t + '_' + training_sentences + '_'\
              + 'V' + filter(str.isdigit, synthesize_version)

# create logger for logging purposes
lgr = logging.getLogger('resultfile')
lgr.setLevel(logging.DEBUG)
# add a file handler
fh = logging.FileHandler('resultfile.log')
fh.setLevel(logging.DEBUG)
# create a formatter and set the formatter for the handler.
frmt = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(frmt)
# add the Handler to the logger
lgr.addHandler(fh)


# Compress the output folder
def zip_dir(zipname, dir_to_zip):
    dir_to_zip_len = len(dir_to_zip.rstrip(os.sep)) + 1
    with zipfile.ZipFile(zipname, mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
        for dirname, subdirs, files in os.walk(dir_to_zip):
            for filename in files:
                path = os.path.join(dirname, filename)
                entry = path[dir_to_zip_len:]
                zf.write(path, entry)


# Disconnect any previous connections to shared folder
while True:
    conn = get_connection_for_unc_directory(share_folder)
    if conn is None:
        break
    else:
        conn.unmount()

# Create a new connection
creds = UncCredentials(r'smi\nabil', '$Password1')
authz_unc = UncDirectory(share_folder, creds)
# check for free drive to use
mnt2 = UncDirectoryMount(authz_unc)
mnt2.mount()

#########################################################
# 1. Create a folder according to the Config file values, if it does not exist
#########################################################
if not os.path.exists(testfolder_path + folder_name):
    os.makedirs(testfolder_path + folder_name)
# Check if folder was created
if not os.path.exists(testfolder_path + folder_name):
    lgr.warn('1. Directory was not created')
    sys.exit()
else:
    lgr.info('1. Directory {0} was created Successfully'.format(folder_name))

#########################################################
# 2. Get the name of all the lab files from the testFolder
#########################################################
lgr.info('2. Getting lab files names from testFolder')
files_names = glob.glob(testfolder_path + '\*.lab')
lgr.info('Files found: {0}'.format(files_names))
# if there are no files in TestFolder then exit
if files_names == "":
    lgr.warn('TestFolder is empty')
    sys.exit()

for file_name in files_names:
    lgr.info('##########################################\n'
             'Synthesizing file: {0}'.format(file_name))
    start_time = datetime.datetime.now().replace(microsecond=0)

    ##########################################
    # 3. Clean the Res folder for the new run
    ##########################################
    lgr.info('3. Deleting res folder content')
    for item in os.listdir(synthesis_path + 'res'):
        os.remove(synthesis_path + 'res//' + item)
    # Check if res folder is empty
    if os.listdir(synthesis_path + 'res'):
        lgr.warn('Res folder was not cleaned')
    else:
        lgr.info('Res folder is empty')

    #########################################################
    # 4. Delete the old label file "kristin_labels_test.lab" in synthesis folder if exist
    #    in order to replace it with a new one
    #########################################################
    lgr.info('4. Deleting the current "kristin_labels_test.lab" in synthesis in order to replace it with a new one')
    if os.path.exists(synthesis_path + 'kristin_labels_test.lab'):
        os.remove(synthesis_path + 'kristin_labels_test.lab')
    # Check if file was deleted
    if os.path.exists(synthesis_path + 'kristin_labels_test.lab'):
        lgr.warn('kristin_labels_test.lab was not deleted')
    else:
        lgr.info('kristin_labels_test.lab is deleted')

    #########################################################
    # 5. Copy a label file from "testFolder" to the labels directory and rename it to "kristin_labels_test.lab"
    #########################################################
    lgr.info('5. Copying a label file from "testFolder" to synthesis directory,rename it to "kristin_labels_test.lab"')
    shutil.copy2(file_name, synthesis_path + 'kristin_labels_test.lab')
    # Check if "kristin_labels_test.lab" was copied successfully
    if not os.path.exists(synthesis_path + 'kristin_labels_test.lab'):
        lgr.warn('kristin_labels_test.lab was not copied')
        continue
    else:
        lgr.info('kristin_labels_test.lab was copied successfully')

    #########################################################
    # 6. Remove the older output of the extract_lexical_features.bat in synthesis directory, if exist
    # Kristin_labels_test.features.txt, Kristin_labels_test.features.output.txt
    #########################################################
    lgr.info('6. Removing the older output of the extract_lexical_features.bat in synthesis directory, if exist '
             'Kristin_labels_test.features.txt, Kristin_labels_test.features.output.txt')
    if os.path.exists(synthesis_path + 'Kristin_labels_test.features.txt'):
        os.remove(synthesis_path + 'Kristin_labels_test.features.txt')
    if os.path.exists(synthesis_path + 'Kristin_labels_test.features.output.txt'):
        os.remove(synthesis_path + 'Kristin_labels_test.features.output.txt')
    # Check if these files were deleted successfully
    if os.path.exists(synthesis_path + 'Kristin_labels_test.features.txt') or \
            os.path.exists(synthesis_path + 'Kristin_labels_test.features.output.txt'):
        lgr.warn('One or both files were not deleted')
    else:
        lgr.info('Files were deleted successfully')

    #########################################################
    # 7. Run extract lexical features bat file
    #########################################################
    lgr.info('7. Running extract_lexical_features.bat')
    os.chdir(synthesis_path)
    # process = subprocess.Popen(synthesis_path + 'extract_lexical_features.bat', stdout=subprocess.PIPE)
    # out, err = process.communicate()
    # lgr.info(out)

    process = subprocess.Popen(synthesis_path + 'extract_lexical_features.bat')
    process.wait()
    lgr.info('Process extract_lexical_features.bat is finished')

    #########################################################
    # 8. Run test_hybrid_synthesis.bat
    #########################################################
    lgr.info('8. Running test_hybrid_synthesis.bat')
    # process = subprocess.Popen(synthesis_path + 'test_hybrid_synthesis.bat', stdout = subprocess.PIPE)
    # out, err = process.communicate()
    # lgr.info(out)

    process = subprocess.Popen(synthesis_path + 'test_hybrid_synthesis.bat')
    process.wait()
    lgr.info('Process test_hybrid_synthesis.bat is finished')

    #########################################################
    # 9. Copy the synthesized wav files from the "res" folder to the "testFolder" folder
    #########################################################
    lgr.info('9. Copying the synthesized wav files from the "res" folder to the "testFolder" folder')
    lgr.info('moving wav files in "Res" folder to {0}'.format(folder_name))
    # move wav files in "Res" folder to the output folder
    for item in os.listdir(synthesis_path + 'res'):
        base, ext = os.path.splitext(item)
        if ext == '.wav':
            s = os.path.join(synthesis_path + 'res', item)
            item = item.split('.')
            d = os.path.join(testfolder_path + folder_name, item[0] + '.' + item[1] + file_suffix + '.wav')
            shutil.move(s, d)

    # Time took to synthesize the file
    end_time = datetime.datetime.now().replace(microsecond=0)
    # gather the files and their run time to be displayed at the end of the log file
    run_time[file_name] = end_time - start_time
    lgr.info('time taken to process this file: {0}'.format(run_time[file_name]))


# Compress the output folder
zip_dir(testfolder_path + folder_name + '.zip', testfolder_path + folder_name)

# send the compressed file to the share folder
print ('Uploading the compressed folder...')
lgr.info('Uploading the compressed folder...')

drive = str(mnt2.disk_drive)
drive += '\\'
print (folder_name)

shutil.copy2(testfolder_path + folder_name + '.zip', drive + folder_name + '.zip')
# check if file was uploaded successfully
print ('upload path check: {0}'.format(str(mnt2.disk_drive) + folder_name + '.zip'))
if os.path.exists(drive + folder_name + '.zip'):
    lgr.info('Compressed file was uploaded Successfully')
else:
    lgr.warn('Compressed file upload is failed')

# Conclude the proccessed files and it's run time
lgr.info('All files run times:')
for k, v in run_time.items():
    lgr.info('File: {0}, Time: {1}  '.format(k, v))

# Stop the connection (unmount)
lgr.info('Unmounting shared folder')
mnt2.unmount()
