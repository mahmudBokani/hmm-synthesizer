# README #

This script is for the Synthesizer tool in Windows.

### Required installation to run this script: ###
Need to install "win_unc" in order to send the compressed folder to the network shared folder
Installation command: pip install win_unc
The script mounts the network shared folder on a free local drive at the beginning and unmount it when it finishes.

### Before running the script, these folder and files must exist: ###
“conf.txt” file, script renames the output files and the new folder according to the values in this file.
Script must be placed beside "synthesis" folder.
Script would process the lab files from "testFolder" and also save the output to this folder.

### More info about the script: ###
After running this script a log file will be created, this file lists the script run step by step.
The script also does check after each step if it was processed successfully.
The script will also calculate how much time it took to synthesize each lab file, displaying these times when finishing each file and also list the files and their process time at the end when finishing synthesizing all the files.

### Script run steps: ###
1. Create a folder according to the Config file values, if it does not exist.
2. Get the name of all the lab files from "testFolder".
3. Clean the "Res" folder for the new run.
4. Delete the old label file "kristin_labels_test.lab" in "synthesis" folder if exist in order to replace it with a new one.
5. Copy a label file from "testFolder" to the labels directory and rename it to "kristin_labels_test.lab".
6. Remove the older output of "extract_lexical_features.bat" in "synthesis" directory, if exist:
   Kristin_labels_test.features.txt, Kristin_labels_test.features.output.txt
7. Run "extract lexical features bat" file.
8. Run "test_hybrid_synthesis.bat".
9. Copy the synthesized wav files from "res" folder to "testFolder" folder.
10. Compress the output folder
11. Send the compressed file to the share folder


